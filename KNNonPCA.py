import time
start_time = time.time()

import pandas as pd
import numpy as np
import sys
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
import math
# %matplotlib inline
np.set_printoptions(threshold=np.inf)


filename = 'Datasets/mnist_train.csv'
trainDataset = pd.read_csv(filename,skipinitialspace=True)

filename = 'Datasets/mnist_test.csv'
testDataset = pd.read_csv(filename,skipinitialspace=True)

array = trainDataset.values
XTrain = array[:,1:785]
YTrain = array[:,0]

array = testDataset.values
XTest = array[:,1:785]
YTest = array[:,0]

# print(np.cov(np.transpose(XTrain)))

# X = scale(XTrain)
pca = PCA()
pca.n_components = 50
pca.fit(XTrain)

var = pca.explained_variance_
# var1=np.cumsum(np.round(pca.explained_variance_ratio_, decimals=4)*100)
# print(np.sort(var))
# print(pca.components_ )
print(var)
print(len(var))
# sys.exit()
print("calculated var...moving forward")

X_reduced_train = pca.transform(XTrain)
X_reduced_test = pca.transform(XTest)
print("calculated new X....calculating KNN now")

###################################################################################   KNN from here

import pandas
import sys
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import KFold
from sklearn.svm import SVC

# sys.exit()

# kf = KFold(n_splits=10)
# kf.get_n_splits(X)
# knn_accucray_array =[]
# for train_index, test_index in kf.split(X):
    # print ("train and test")
    # print train_index
    # print test_index
knn = KNeighborsClassifier(n_neighbors=1)
knn.fit(X_reduced_train,YTrain)

print("KNN accuracy score below:  ")
testPredictions = knn.predict(X_reduced_test)
print("test accuracy_score", accuracy_score(YTest, testPredictions)*100)
print(confusion_matrix(YTest, testPredictions))

print("in the end")
print("--- %s seconds ---" % (time.time() - start_time))