from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
import math

def PCA2(XTrain, XTest):
	pca = PCA()
	pca.n_components = 50
	pca.fit(XTrain)

	var= pca.explained_variance_
	# print(var)
	# print(len(var))

	# pca.n_components = 50

	X_reduced_train = pca.transform(XTrain)
	# X_reduced_train = pca.inverse_transform(X_reduced_train)

	print('calculated X_reduced_train')



	X_reduced_test = pca.transform(XTest)
	# X_reduced_test = pca.fit_transform(X_reduced_test)

	print('calculated X_reduced_test')
	return X_reduced_train, X_reduced_test