import pandas
import sys
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

filename = 'Datasets/mnist_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)

array = trainDataset.values
XTrain = array[1:50,1:785]
YTrain = array[1:50,0]
print(XTrain.shape)
print(YTrain.shape)

array = testDataset.values
XTest = array[1:50,1:785]
YTest = array[1:50,0]

# # KNN
# import KNN
# predictions = KNN.KNN(XTrain,YTrain,XTest,YTest)
# print(accuracy_score(YTest, predictions))

# print('pca starting....')
# #PCA
# import PCA2
# X_reduced_train, X_reduced_test  = PCA2.PCA2(XTrain, XTest)
# print(X_reduced_train.shape)
# print(X_reduced_test.shape)
# predictions = KNN.KNN(X_reduced_train,YTrain,X_reduced_test,YTest)
# print(accuracy_score(YTest, predictions))

#Direction Features
print('reading data comleted....imporing Dirfeat')
import DirFeat
XTrain1 = DirFeat.DirFeat(XTrain, 1)
print('back to main funct')
print(XTrain1.shape)