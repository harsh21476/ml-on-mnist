from sklearn.neighbors import KNeighborsClassifier
def KNN(XTrain,YTrain,XTest,YTest):
	knn = KNeighborsClassifier(n_neighbors=3)
	knn.fit(XTrain,YTrain)
	predictions = knn.predict(XTest)
	return predictions