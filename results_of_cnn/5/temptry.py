import matplotlib
matplotlib.use('Agg')

import time
start_time = time.time()

import tensorflow as tf
print("imported tf")
import pandas
import sys
from sklearn import preprocessing
from pandas.tools.plotting import scatter_matrix
# import matplotlib.pyplot as plt
# from sklearn import model_selection
# from sklearn.metrics import classification_report
# from sklearn.metrics import confusion_matrix
# from sklearn.metrics import accuracy_score
# from sklearn.linear_model import LogisticRegression
# from sklearn.tree import DecisionTreeClassifier
# from sklearn.neighbors import KNeighborsClassifier
# from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
# from sklearn.naive_bayes import GaussianNB
# from sklearn.svm import SVC
import matplotlib.pyplot as plt
import numpy as np
print("impor others")
# from tensorflow.examples.tutorials.mnist import input_data
# mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
##################################################################
filename = 'Datasets/mnist_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)
print("imported datasets")
array = trainDataset.values
XTrain = array[:,1:785]
YTrain1 = array[:,0]
# print(XTrain.shape)
# print(YTrain1.shape)
array = testDataset.values
XTest = array[:,1:785]
YTest1 = array[:,0]
##########################################################
filename = 'elastic_distortion/mnist_elastic_distorted_01.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)
array = trainDataset.values
temp_trainX = array[:,0:784]
XTrain = np.vstack((XTrain,temp_trainX))
temp_trainY = array[:,784]
YTrain1 = np.hstack((YTrain1,temp_trainY))

for i in range(12):
	print("stacking arrays")
	filename = 'elastic_distortion/mnist_elastic_distorted_'+ str('%02d'%(i+1)) +'.csv'
	trainDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)
	array = trainDataset.values
	temp_trainX = array[:,0:784]
	XTrain = np.vstack((XTrain,temp_trainX))
	temp_trainY = array[:,784]
	YTrain1 = np.hstack((YTrain1,temp_trainY))

	filename = 'rotation_distortion/mnist_rotation_distorted_'+str('%02d'%(i+1))+'.csv'
	trainDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)
	array = trainDataset.values
	temp_trainX = array[:,0:784]
	XTrain = np.vstack((XTrain,temp_trainX))
	temp_trainY = array[:,784]
	YTrain1 = np.hstack((YTrain1,temp_trainY))


###########################################################
lb = preprocessing.LabelBinarizer()
lb.fit([0,1,2,3,4,5,6,7,8,9])
YTrain = lb.transform(YTrain1)
YTest = lb.transform(YTest1)


XTrain = XTrain.astype(float)
YTrain = YTrain.astype(float)
XTest = XTest.astype(float)
YTest = YTest.astype(float)

XTrain = XTrain/255
XTest = XTest/255

# sess = tf.InteractiveSession()
print("importing datasets done")


x = tf.placeholder(tf.float32, shape=[None, 784])
y_ = tf.placeholder(tf.float32, shape=[None, 10])

# sess = tf.Session()
# ////////////////////////////////////////////////////////////////////////////
# W = tf.Variable(tf.zeros([784,10]))
# b = tf.Variable(tf.zeros([10]))
# print("")

# sess.run(tf.global_variables_initializer())

# y = tf.matmul(x,W) + b
# cross_entropy = tf.reduce_mean(
#     tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))

# train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

# for _ in range(1000):
#   batch = mnist.train.next_batch(100)
#   train_step.run(feed_dict={x: batch[0], y_: batch[1]})

# correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

# accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# print(accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
# //////////////////////////////////////////////////////////////////////////////////
testing_results = np.array([0])
epoch_number = np.array([0])

def next_batch(num, data, labels):
	'''
	Return a total of `num` random samples and labels. 
	'''
	idx = np.arange(0 , len(data))
	np.random.shuffle(idx)
	idx = idx[:num]
	data_shuffle = [data[ i] for i in idx]
	labels_shuffle = [labels[ i] for i in idx]
	return np.asarray(data_shuffle), np.asarray(labels_shuffle)


# batch = next_batch(1, XTrain, YTrain)
# print(batch[1])
# plt.imshow((batch[0].reshape(28,28)), interpolation='nearest')
# plt.show()



def weight_variable(shape):
	initial = tf.truncated_normal(shape, stddev=0.1)
	return tf.Variable(initial)

def bias_variable(shape):
	initial = tf.constant(0.1, shape=shape)
	return tf.Variable(initial)

def conv2d(x, W):
	return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')

def max_pool_2x2(x):
	return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='VALID')

x_image = tf.reshape(x, [-1, 28, 28, 1])

W_conv1 = weight_variable([5, 5, 1, 25])
b_conv1 = bias_variable([25])

h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

W_conv2 = weight_variable([5, 5, 25, 50])
b_conv2 = bias_variable([50])

h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

W_fc1 = weight_variable([4 * 4 * 50, 100])
b_fc1 = bias_variable([100])

h_pool2_flat = tf.reshape(h_pool2, [-1, 4*4*50])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

W_fc2 = weight_variable([100, 10])
b_fc2 = bias_variable([10])

y_conv = tf.matmul(h_fc1, W_fc2) + b_fc2

saver = tf.train.Saver()
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
train_step = tf.train.GradientDescentOptimizer(0.000001).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


EPOCH = 800
BATCH_SIZE = 50
ITERATIONS_PERePOCH = int(((YTrain1.shape)[0])/BATCH_SIZE)
print("ITERATIONS_PERePOCH",ITERATIONS_PERePOCH)
with tf.Session() as sess:
	sess.run(tf.global_variables_initializer())
	for j in range(EPOCH):			
		for i in range(ITERATIONS_PERePOCH):
			batch = next_batch(BATCH_SIZE, XTrain, YTrain)
			# batch = mnist.train.next_batch(1)
			if i % 5000 == 0:
				# train_accuracy = accuracy.eval(feed_dict={x: batch[0], y_: batch[1]})
				print('EPOCH = %d, Iteration = %d, PercentOfEpoch = %d'%(j, i, (i*100/ITERATIONS_PERePOCH)) )

				# print('epoch %d, training accuracy %g' % (i/55000, train_accuracy))
				# print('epoch %d, training error %g' % (i/55000, (100-train_accuracy*100)))
				test_accuracy = accuracy.eval(feed_dict={x: XTest, y_: YTest})
				print('Test accuracy %g' % test_accuracy)
				print(" ")
				
				filename = open("results.txt", "a")
				datatobewritten = ('Epoch', j , "PercentOfEpoch", (i*100/ITERATIONS_PERePOCH), "Test Accuracy", test_accuracy)
				s = str(datatobewritten)
				filename.write(s+"\n")
				filename.close()


				# print(test_accuracy)
				testing_results =  np.append(testing_results, (100-test_accuracy*100))
				epoch_number = np.append(epoch_number,j)
				# print(epoch_number,testing_results)
				plt.plot(epoch_number[1:],testing_results[1:])
				plt.xlabel('Epoch')
				plt.ylabel('Test Error')
				plt.savefig('graph.png',format='png')
			train_step.run(feed_dict={x: batch[0], y_: batch[1]})
		print("--- %s seconds ---" % (time.time() - start_time))
	# print('test accuracy %g' % accuracy.eval(feed_dict={x: XTest, y_: YTest}))
	save_path = saver.save(sess, "savedmodel/model.ckpt")
	print("Model saved in file: %s" % save_path)	
	# a1 = sess.run(W_conv1)
	# b1 = sess.run(b_conv1)
	# print(a1,b1)

