import matplotlib
from sklearn.metrics import accuracy_score
# from tensorboard.plugins import graph

matplotlib.use('Agg')
import _pickle as cPickle
import time

start_time = time.time()

import tensorflow as tf

print("imported tf")
import pandas
import sys
from sklearn import preprocessing
# from pandas.tools.plotting import scatter_matrix
from sklearn.svm import SVC
# import matplotlib.pyplot as plt
# from sklearn import model_selection
# from sklearn.metrics import classification_report
# from sklearn.metrics import confusion_matrix
# from sklearn.metrics import accuracy_score
# from sklearn.linear_model import LogisticRegression
# from sklearn.tree import DecisionTreeClassifier
# from sklearn.neighbors import KNeighborsClassifier
# from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
# from sklearn.naive_bayes import GaussianNB
# from sklearn.svm import SVC
import matplotlib.pyplot as plt
import numpy as np

print("import others")
# from tensorflow.examples.tutorials.mnist import input_data
# mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
##################################################################
filename = 'Datasets/mnist_train.csv'
trainDataset = pandas.read_csv(filename, skipinitialspace=True, header=None)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename, skipinitialspace=True, header=None)
print("imported datasets")
array = trainDataset.values
XTrain = array[:, 1:785]
YTrain1 = array[:, 0]
# print(XTrain.shape)
# print(YTrain1.shape)
array = testDataset.values
XTest = array[:, 1:785]
YTest1 = array[:, 0]
##########################################################
# filename = 'elastic_distortion/mnist_elastic_distorted_01.csv'
# trainDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)
# array = trainDataset.values
# temp_trainX = array[:,0:784]
# XTrain = np.vstack((XTrain,temp_trainX))
# temp_trainY = array[:,784]
# YTrain1 = np.hstack((YTrain1,temp_trainY))

# for i in range(12):
# 	print("stacking arrays")
# 	filename = 'elastic_distortion/mnist_elastic_distorted_'+ str('%02d'%(i+1)) +'.csv'
# 	trainDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)
# 	array = trainDataset.values
# 	temp_trainX = array[:,0:784]
# 	XTrain = np.vstack((XTrain,temp_trainX))
# 	temp_trainY = array[:,784]
# 	YTrain1 = np.hstack((YTrain1,temp_trainY))

# 	filename = 'rotation_distortion/mnist_rotation_distorted_'+str('%02d'%(i+1))+'.csv'
# 	trainDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)
# 	array = trainDataset.values
# 	temp_trainX = array[:,0:784]
# 	XTrain = np.vstack((XTrain,temp_trainX))
# 	temp_trainY = array[:,784]
# 	YTrain1 = np.hstack((YTrain1,temp_trainY))


###########################################################
lb = preprocessing.LabelBinarizer()
lb.fit([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
YTrain = lb.transform(YTrain1)
YTest = lb.transform(YTest1)

XTrain = XTrain.astype(float)
YTrain = YTrain.astype(float)
XTest = XTest.astype(float)
YTest = YTest.astype(float)

XTrain = XTrain / 255
XTest = XTest / 255
# /////////////////////////////

# sess = tf.InteractiveSession()
print("importing datasets done")
print("importing datasets done")
# sess = tf.Session()
# ////////////////////////////////////////////////////////////////////////////
# W = tf.Variable(tf.zeros([784,10]))
# b = tf.Variable(tf.zeros([10]))
# print("")

# sess.run(tf.global_variables_initializer())

# y = tf.matmul(x,W) + b
# cross_entropy = tf.reduce_mean(
#     tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))

# train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

# for _ in range(1000):
#   batch = mnist.train.next_batch(100)
#   train_step.run(feed_dict={x: batch[0], y_: batch[1]})

# correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

# accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# print(accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
# //////////////////////////////////////////////////////////////////////////////////
testing_results = np.array([0])
epoch_number = np.array([0])


def next_batch(num, data, labels):
    '''
	Return a total of `num` random samples and labels. 
	'''
    idx = np.arange(0, len(data))
    np.random.shuffle(idx)
    idx = idx[:num]
    data_shuffle = [data[i] for i in idx]
    labels_shuffle = [labels[i] for i in idx]
    return np.asarray(data_shuffle), np.asarray(labels_shuffle)


#     =========================     predict the unknown sample's output from the stored model      ====================
with tf.Session() as session:
    saver = tf.train.import_meta_graph('./savedmodel/model.ckpt.meta')
    saver.restore(session,tf.train.latest_checkpoint('./savedmodel/'))
    # graph = tf.get_default_graph()
    # saver.restore(session, ckpt.model_checkpoint_path)
    session.run(tf.global_variables_initializer())
    graph=tf.get_default_graph()
    print([tensor.name for tensor in tf.get_default_graph().as_graph_def().node])
    x = graph.get_tensor_by_name("Placeholder:0")
    # y_ = graph.get_tensor_by_name("Placeholder_1:0")
    number_of_train_samples = XTrain.shape[0]
    number_of_test_samples = XTest.shape[0]
    # number_of_train_samples = 5000
    # number_of_test_samples = 5000
    
    # feed_dict = {x: XTest}
    # y_conv = graph.get_tensor_by_name("MatMul_1:0")
    y_extracted = graph.get_tensor_by_name("Relu_2:0")
    feed_dict = {x: np.reshape(XTrain[0:number_of_train_samples,:], (number_of_train_samples, 784))}
    cnn_train_feature_extraction = session.run(y_extracted,feed_dict)
    cnn_test_feature_extraction = session.run(y_extracted, feed_dict={x: np.reshape(XTest[0:number_of_test_samples,:],(number_of_test_samples,784))})
    # y_conv_run = session.run(y_conv, feed_dict={x: np.reshape(XTest[0:number_of_test_samples,:],(number_of_test_samples,784))})
    # print(y_conv_run.shape)

    # correct_prediction = tf.equal(tf.argmax(y_conv_run, 1), tf.argmax(YTest[:number_of_test_samples,:], 1))
    # cnn_test_accuracy = accuracy_score(np.argmax(YTest[0:number_of_test_samples,:], 1),np.argmax(np.reshape(y_conv_run,(number_of_test_samples,10)),1))
    # accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    # cnn_test_accuracy = accuracy.eval(feed_dict={x: XTest[0:number_of_test_samples,:], y_: YTest[0:number_of_test_samples,:]})
    # print(cnn_test_accuracy)
    # cnn_test_accuracy = accuracy_score(tf.argmax(YTest1, 1),tf.argmax(y_conv_run,1))
    # accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    # cnn_feature_extraction = session.run(y_extracted, feed_dict)
    # correct_prediction = tf.equal(tf.argmax(y_cnn_predicted, 1), tf.argmax(y_, 1))
    # y_cnn_predicted = sess.run(tf.argmax(y_,1),feed_dict={x: np.reshape(XTrain,(number_of_train_samples,784))})
    # cnn_test_accuracy = accuracy.eval(feed_dict={x: XTest, y_: YTest})

print("svm training....")
# print(2**(-11))
# print(float(2e-11))
clf = SVC(C=128, gamma=(2**(-11)), probability=True, tol=1e-3, max_iter=-1, decision_function_shape='ovo')
# clf=SVC(C=128,gamma=2e-11,probability=True,tol=1e-3,max_iter=-1,decision_function_shape='ovo')
clf.fit(cnn_train_feature_extraction,YTrain1[0:number_of_train_samples])

svm_accuracy = clf.score(cnn_test_feature_extraction,YTest1[0:number_of_test_samples])
# print("Accuracy of CNN-ONLY: ")
# print(cnn_test_accuracy)
print ('Accuracy of Hybrid SVM: ')
print (svm_accuracy)


with open('my_dumped_classifier.pkl', 'wb') as fid:
    cPickle.dump(clf, fid)   


# svm_model_file = 'my_dumped_classifier_1.pickle'
# svm_model_clf = cPickle.load(open(svm_model_file,'rb'))
# print 'predicted output: ',svm_model_clf.predict(cnn_feature_extraction)
# print 'original output: ',YTest1[0:50]






# svm_accuracy = svm_model_clf.score(cnn_feature_extraction,YTest1[0])
# svm_accuracy = svm_model_object.score(XSVMTest,YSVMTest)
# print 'Accuracy of Hybrid SVM: '
# print svm_accuracy

# =====================================================================================================================


# ================================      Accuracy        ==========================================
# correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
# accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
# ===============================================================================================