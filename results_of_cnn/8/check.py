import _pickle as cPickle

svm_model_file = 'my_dumped_classifier_0.pkl'
svm_model_file_object = open(svm_model_file,'rb')
svm_clf = cPickle.load(svm_model_file_object)

print(svm_clf)

outfile = 'my_dumped_classifier_1.pickle'
with open(outfile, 'wb') as pickle_file:
    cPickle.dump(svm_clf, pickle_file,2)
#
# svm_model_file_object.close()