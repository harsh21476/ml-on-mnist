import numpy as np
import pandas
from tensorflow.contrib.learn.python.learn.datasets import base
import sklearn.preprocessing as preprocessing
import tensorflow as tf
from sklearn.svm import SVC

from tensorflow.examples.tutorials.mnist import input_data

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='VALID')

def next_batch(num, data, labels):
    '''
    Return a total of `num` random samples and labels. 
    '''
    idx = np.arange(0 , len(data))
    np.random.shuffle(idx)
    idx = idx[:num]
    data_shuffle = [data[ i] for i in idx]
    labels_shuffle = [labels[ i] for i in idx]
    return np.asarray(data_shuffle), np.asarray(labels_shuffle)


filename = 'Datasets/mnist_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True,header=None)
print("imported datasets")
array = trainDataset.values
XTrain = array[:,1:785]
YTrain1 = array[:,0]
# print(XTrain.shape)
# print(YTrain1.shape)
array = testDataset.values
XTest = array[:,1:785]
YTest1 = array[:,0]
# sys.exit()
lb = preprocessing.LabelBinarizer()
lb.fit([0,1,2,3,4,5,6,7,8,9])
YTrain = lb.transform(YTrain1)
YTest = lb.transform(YTest1)
XTrain = XTrain.astype(np.float32)
XTrain=XTrain/255
YTrain = YTrain.astype(np.float64)
XTest = XTest.astype(np.float32)
XTest=XTest/255
YTest = YTest.astype(np.float64)
# sess = tf.InteractiveSession()
print("importing datasets done")

temp=np.hstack((XTrain,YTrain))
print(XTrain.shape)
print (YTrain.shape)
np.savetxt("my_mnist.csv",temp, delimiter=",",fmt='%.6f')






train_filename = 'Datasets/mnist_train.csv'
mnist=base.load_csv_without_header(filename=train_filename,target_dtype=np.float32,features_dtype=np.float32,target_column=784)
# mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

print("importing datasets done")

import time
start_time = time.time()


sess = tf.InteractiveSession()

BATCH_SIZE=1

x = tf.placeholder(np.float32, shape=[None, 784])
y_ = tf.placeholder(np.float32, shape=[None, 10])

sess = tf.Session()
# ////////////////////////////////////////////////////////////////////////////
# W = tf.Variable(tf.zeros([784,10]))
# b = tf.Variable(tf.zeros([10]))
# print("")

# sess.run(tf.global_variables_initializer())

# y = tf.matmul(x,W) + b
# cross_entropy = tf.reduce_mean(
#     tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))

# train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

# for _ in range(1000):
#   batch = mnist.train.next_batch(100)
#   train_step.run(feed_dict={x: batch[0], y_: batch[1]})

# correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

# accuracy = tf.reduce_mean(tf.cast(correct_prediction, np.float32))

# print(accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
# //////////////////////////////////////////////////////////////////////////////////


x_image = tf.reshape(x, [-1, 28, 28, 1])

W_conv1 = weight_variable([5, 5, 1, 25])
b_conv1 = bias_variable([25])

h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

W_conv2 = weight_variable([5, 5, 25, 50])
b_conv2 = bias_variable([50])

h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

W_fc1 = weight_variable([4 * 4 * 50, 100])
b_fc1 = bias_variable([100])

h_pool2_flat = tf.reshape(h_pool2, [-1, 4*4*50])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

W_fc2 = weight_variable([100, 10])
b_fc2 = bias_variable([10])

y_conv = tf.matmul(h_fc1, W_fc2) + b_fc2

saver = tf.train.Saver()
filename = open("results.txt", "a")
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, np.float32))

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for i in range(27500000):
    # for i in range(10):
        # for i in range(500):
        batch = next_batch(BATCH_SIZE, XTrain, YTrain)
        # batch = mnist.train.next_batch(1)
        if i % 55000 == 0:
            train_accuracy = accuracy.eval(feed_dict={x: batch[0], y_: batch[1]})
            print('epoch %d, training accuracy %g' % (i/55000, train_accuracy))
            print('epoch %d, error %g' % (i/55000, (100-train_accuracy*100)))
            test_accuracy = accuracy.eval(feed_dict={ x: XTest, y_: YTest})
            print('test accuracy %g' % test_accuracy)
            print("--- %s seconds ---" % (time.time() - start_time))
            datatobewritten = ('Epoch', (i/55000) , "Test Accuracy", test_accuracy)
            s = str(datatobewritten)
            filename.write(s)
        if i % 10000 == 0:
            print(i)
        # if (i % 100) == 0:
        # 	print(i)
        train_step.run(feed_dict={x: batch[0], y_: batch[1]})
        # ============= to predict the output within the session ==================
        # print sess.run(tf.argmax(y_conv,1),feed_dict={x: np.reshape(XTrain[0],(1,784))})
        # ==========================================================================
        # print('test accuracy %g' % accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
    save_path = saver.save(sess, "savedmodel/model.ckpt")
    print("Model saved in file: %s" % save_path)





    # ==================            SVM Data Creation           ============================
    svm_train_dataset=np.zeros((1,101),dtype=np.float64)
    # for i in range((XTrain.shape[0])):
    # sample_XSVMTrain = sess.run(h_fc1,feed_dict={x: XTrain})
    sample_XSVMTrain = sess.run(h_fc1,feed_dict={x: XTrain})
    # sample_YSVMTrain = YTrain1
    sample_YSVMTrain = np.reshape(YTrain1,(YTrain1.shape[0],1))
    new_svm_train_feature = np.hstack((sample_XSVMTrain,sample_YSVMTrain))
    svm_train_dataset=np.vstack((svm_train_dataset,new_svm_train_feature))
    svm_train_dataset=np.delete(svm_train_dataset,[0],0)
    np.savetxt("svm_mnist_train.csv", svm_train_dataset, delimiter=",", fmt='%d')

    svm_test_dataset=np.zeros((1,101),dtype=np.float64)
    # for i in range((XTest.shape[0])):
    # sample_XSVMTest = sess.run(h_fc1,feed_dict={x: XTest})
    sample_XSVMTest = sess.run(h_fc1,feed_dict={x: XTest})
    # sample_YSVMTest = YTest1
    sample_YSVMTest = np.reshape(YTest1,(YTest1.shape[0],1))
    new_svm_test_feature = np.hstack((sample_XSVMTest,sample_YSVMTest))
    svm_test_dataset=np.vstack((svm_test_dataset,new_svm_test_feature))
    svm_test_dataset = np.delete(svm_test_dataset, [0], 0)
    np.savetxt("svm_mnist_test.csv", svm_test_dataset, delimiter=",", fmt='%d')
    # =================================================================================

# ====================          SVM Training Phase           =============================
XSVMTrain = np.array(svm_train_dataset[:,0:100])
YSVMTrain = np.array(svm_train_dataset[:,100])
XSVMTest = np.array(svm_test_dataset[:,0:100])
YSVMTest = np.array(svm_test_dataset[:,100])

# Change 'max_iter' in the below step, if it takes more time. lets us say max_iter = 3000 or 30000.
# Don't change rest of the parameters as of now.
clf=SVC(C=128,gamma=2e-11,probability=True,tol=1e-3,max_iter=-1,decision_function_shape='ovo')
clf.fit(XSVMTrain,YSVMTrain)

svm_accuracy = clf.score(XSVMTest,YSVMTest)
print('Accuracy of Hybrid SVM: ')
print(svm_accuracy)




# ===================================================================================

    # a1 = sess.run(W_conv1)
    # b1 = sess.run(b_conv1)
    # print(a1,b1)

#     =========================     predict the unknown sample's output from the stored model      ====================
# with tf.Session(graph=graph) as session:
#     ckpt = tf.train.get_checkpoint_state('./model/')
#     saver.restore(session, ckpt.model_checkpoint_path)
#     feed_dict = {tf_train_dataset: batch_data}
#     predictions = session.run([test_prediction], feed_dict)
# =====================================================================================================================



filename.close()