import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

print("importing datasets done")

import time
start_time = time.time()

import tensorflow as tf
sess = tf.InteractiveSession()

x = tf.placeholder(tf.float32, shape=[None, 784])
y_ = tf.placeholder(tf.float32, shape=[None, 10])

sess = tf.Session()
# ////////////////////////////////////////////////////////////////////////////
# W = tf.Variable(tf.zeros([784,10]))
# b = tf.Variable(tf.zeros([10]))
# print("")

# sess.run(tf.global_variables_initializer())

# y = tf.matmul(x,W) + b
# cross_entropy = tf.reduce_mean(
#     tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))

# train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

# for _ in range(1000):
#   batch = mnist.train.next_batch(100)
#   train_step.run(feed_dict={x: batch[0], y_: batch[1]})

# correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

# accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# print(accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
# //////////////////////////////////////////////////////////////////////////////////
testing_results = np.array([0])
epoch_number = np.array([0])

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='VALID')

x_image = tf.reshape(x, [-1, 28, 28, 1])

W_conv1 = weight_variable([5, 5, 1, 25])
b_conv1 = bias_variable([25])

h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

W_conv2 = weight_variable([5, 5, 25, 50])
b_conv2 = bias_variable([50])

h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

W_fc1 = weight_variable([4 * 4 * 50, 100])
b_fc1 = bias_variable([100])

h_pool2_flat = tf.reshape(h_pool2, [-1, 4*4*50])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

W_fc2 = weight_variable([100, 10])
b_fc2 = bias_variable([10])

y_conv = tf.matmul(h_fc1, W_fc2) + b_fc2

saver = tf.train.Saver()
filename = open("results.txt", "a")
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
train_step = tf.train.GradientDescentOptimizer(0.00001).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


with tf.Session() as sess:
	sess.run(tf.global_variables_initializer())
	for i in range(27500000):
	# for i in range(500):
		batch = mnist.train.next_batch(1)
		if i % 55000 == 0:
			# train_accuracy = accuracy.eval(feed_dict={x: batch[0], y_: batch[1]})
			# print('epoch %d, training accuracy %g' % (i/55000, train_accuracy))
			# print('epoch %d, error %g' % (i/55000, (100-train_accuracy*100)))
			test_accuracy = accuracy.eval(feed_dict={ x: mnist.test.images, y_: mnist.test.labels})
			# print('test accuracy %g' % test_accuracy)
			
			datatobewritten = ('Epoch', (i/55000) , "Test Accuracy", test_accuracy)
			s = str(datatobewritten)
			filename.write(s + "\n")

			print(test_accuracy)
			testing_results =  np.append(testing_results, (100-test_accuracy*100))
			epoch_number = np.append(epoch_number,i/55000)
			# print(epoch_number,testing_results)
			plt.plot(epoch_number[1:],testing_results[1:])
			plt.xlabel('Epoch')
			plt.ylabel('Testing error')
			plt.savefig('books_read.png',format='png')

			print("--- %s seconds ---" % (time.time() - start_time))
		if i % 10000 == 0:
			print(i)
		# if (i % 100) == 0:
		# 	print(i)
		train_step.run(feed_dict={x: batch[0], y_: batch[1]})
	print('test accuracy %g' % accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
	save_path = saver.save(sess, "savedmodel/model.ckpt")
	print("Model saved in file: %s" % save_path)	
	# a1 = sess.run(W_conv1)
	# b1 = sess.run(b_conv1)
	# print(a1,b1)
filename.close()

