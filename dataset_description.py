import pandas
import numpy as np
import sys
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

filename = 'Datasets/mnist_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)


# shape
print("trainDataset_shape = ",trainDataset.shape)
print("testDataset.shape = ", testDataset.shape)

array = trainDataset.values
XTrain = array[:,1:785]
YTrain = array[:,0]

array = testDataset.values
XTest = array[:,1:785]
YTest = array[:,0]

# Dataset description
# print(range(len(np.unique(YTrain))))
print("XTrain full")
count = 0
for j in range(len(np.unique(YTrain))):
	for i in range(len(YTrain)):
		# print(YTrain[i])
		if YTrain[i]==j:
			# print("zero")
			count = count+1
	print(j , count, (count/(len(YTrain)))*100)
	count = 0

# Dataset description 0-20000
print("XTrain 20000")
count = 0
for j in range(len(np.unique(YTrain[0:20000]))):
	for i in range(len(YTrain[0:20000])):
		# print(YTrain[i])
		if YTrain[i]==j:
			# print("zero")
			count = count+1
	print(j , count, (count/(len(YTrain[0:20000])))*100)
	count = 0


# Dataset description 20000-40000
print("XTrain 20-40")
count = 0
for j in range(len(np.unique(YTrain[20000:40000]))):
	for i in range(len(YTrain[20000:40000])):
		# print(YTrain[i])
		if YTrain[(i+20000)]==j:
			# print("zero")
			count = count+1
	print(j , count, (count/(len(YTrain[20000:40000])))*100)
	count = 0

# Dataset description 40000-60000
print("XTrain 40-60")
count = 0
for j in range(len(np.unique(YTrain[40000:60000]))):
	for i in range(len(YTrain[40000:60000])):
		# print(YTrain[i])
		if YTrain[(i+40000)]==j:
			# print("zero")
			count = count+1
	print(j , count, (count/(len(YTrain[40000:60000])))*100)
	count = 0


# ###################################################### Test Dataset

print("XTest full")
count = 0
for j in range(len(np.unique(YTest))):
	for i in range(len(YTest)):
		# print(YTest[i])
		if YTest[i]==j:
			# print("zero")
			count = count+1
	print(j , count, (count/(len(YTest)))*100)
	count = 0

# Dataset description 0-20000
print("XTest 0-5")
count = 0
for j in range(len(np.unique(YTest[0:5000]))):
	for i in range(len(YTest[0:5000])):
		# print(YTest[i])
		if YTest[i]==j:
			# print("zero")
			count = count+1
	print(j , count, (count/(len(YTest[0:5000])))*100)
	count = 0

# Dataset description 20000-40000
print("XTest 5-10")
count = 0
for j in range(len(np.unique(YTest[5000:10000]))):
	for i in range(len(YTest[5000:10000])):
		# print(YTest[i])
		if YTest[(i+5000)]==j:
			# print("zero")
			count = count+1
	print(j , count, (count/(len(YTest[5000:10000])))*100)
	count = 0

