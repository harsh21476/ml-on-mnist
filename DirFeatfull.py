import pandas
import sys
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
from scipy import ndimage
import pandas
import sys
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC


# filename = 'Datasets/mnist_train.csv'
# trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)

# array = trainDataset.values
# XTrain = array[1:50,1:785]
# YTrain = array[1:50,0]
# print(XTrain.shape)
# print(YTrain.shape)

array = testDataset.values
# XTest = array[1:50,1:785]
# YTest = array[1:50,0]


data = array[8000,1:785]
# data = np.array(data0)
newdata = np.reshape(data,(28,28))
plt.imshow(newdata,cmap='gray')
plt.show()



''' Direction names
# x1y0 =d0
# x1y1 =d1
# x0y1 = d2
# x-1y1 = d3
# x-1y0 = d4
# x-1y-1 = d5
# x0y-1 = d6
# x1y-1 = d7
'''

kernald0 = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernald1 = np.array([[0,1,2],[-1,0,1],[-2,-1,0]])
kernald2 = np.array([[1,2,1],[0,0,0],[-1,-2,-1]])
kernald3 = np.array([[2,1,0],[1,0,-1],[0,-1,-2]])
kernald4 = np.array([[1,0,-1],[2,0,-2],[1,0,-1]])
kernald5 = np.array([[0,-1,-2],[1,0,-1],[2,1,0]])
kernald6 = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
kernald7 = np.array([[-2,-1,0],[-1,0,1],[0,1,2]])
# plt.imshow(c,cmap='gray',vmin = -1,)

# plt.imshow(kernald0,cmap='gray')
# plt.show()
c = ndimage.convolve(newdata,kernald0,mode='constant', cval=0.0)
plt.imshow(c,cmap='gray')
plt.show()

# plt.imshow(kernald1,cmap='gray')
# plt.show()
c = ndimage.convolve(newdata,kernald1,mode='constant', cval=0.0)
plt.imshow(c,cmap='gray')
plt.show()

# plt.imshow(kernald2,cmap='gray')
# plt.show()
c = ndimage.convolve(newdata,kernald2,mode='constant', cval=0.0)
plt.imshow(c,cmap='gray')
plt.show()

# plt.imshow(kernald3,cmap='gray')
# plt.show()
c = ndimage.convolve(newdata,kernald3,mode='constant', cval=0.0)
plt.imshow(c,cmap='gray')
plt.show()

# plt.imshow(kernald4,cmap='gray')
# plt.show()
c = ndimage.convolve(newdata,kernald4,mode='constant', cval=0.0)
plt.imshow(c,cmap='gray')
plt.show()

# plt.imshow(kernald5,cmap='gray')
# plt.show()
c = ndimage.convolve(newdata,kernald5,mode='constant', cval=0.0)
plt.imshow(c,cmap='gray')
plt.show()

# plt.imshow(kernald6,cmap='gray')
# plt.show()
c = ndimage.convolve(newdata,kernald6,mode='constant', cval=0.0)
plt.imshow(c,cmap='gray')
plt.show()

# plt.imshow(kernald7,cmap='gray')
# plt.show()
c = ndimage.convolve(newdata,kernald7,mode='constant', cval=0.0)
plt.imshow(c,cmap='gray')
plt.show()

