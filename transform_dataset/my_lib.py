from PIL import Image

import cv2
from matplotlib import pyplot as plt
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import GaussianNB
from sklearn.decomposition import PCA
import numpy
from scipy.ndimage.interpolation import map_coordinates
from scipy.ndimage.filters import gaussian_filter

def show_image(matrix,size):
    sample_image=np.reshape(matrix,size)
    plt.imshow(sample_image, cmap='gray', interpolation='nearest')
    plt.show()

def my_naive_bayes(XTrain,YTrain,XTest,YTest):
    nb = GaussianNB()

    # Train the model using the training sets
    nb.fit(XTrain, YTrain)

    # Predict Output
    predicted = nb.predict(XTest)

    print(accuracy_score(YTest, predicted))

def rotate_circ_extremes(x,n_times):
    temp=x
    i=np.array([0,1,2,5,8,7,6,3])
    temp.flat[i]=np.roll(a=temp.flat[i],shift=n_times)
    return temp

def my_pca(x,n_components):
    pca = PCA(n_components=n_components)
    x_train_pca = pca.fit_transform(x)


def my_scale_image(sample,scale_x,scale_y):
    im = Image.new('L', (28, 28))
    im.putdata(sample)
    img = cv2.cvtColor(np.array(im), cv2.COLOR_GRAY2RGB)

    h, w = img.shape[:2]  # get h, w of image


    if scale_x > 1 or scale_y > 1:

        scaled_img = cv2.resize(img, None, fx=scale_x, fy=scale_y, interpolation=cv2.INTER_LINEAR)  # scale image
        sh, sw = scaled_img.shape[:2]  # get h, w of scaled image
        center_y = int(sh / 2 - h / 2)
        center_x = int(sw / 2 - w / 2)
        cropped = scaled_img[center_y:center_y + h, center_x:center_x + w]

        result = cropped

    elif scale_x > 0 or scale_y > 0:
        scaled_img = cv2.resize(img, None, fx=scale_x, fy=scale_y, interpolation=cv2.INTER_CUBIC)  # scale image
        result = scaled_img

    else:  # scale_x or scale_y is negative
        print("Scales must be greater than 0; returning the original image.")
        result = img

    if result.shape < img.shape:  # one of the dimensions was scaled smaller, so need to pad
        sh, sw = result.shape[:2]  # get h, w of cropped, scaled image
        center_y = int(h / 2 - sh / 2)
        center_x = int(w / 2 - sw / 2)
        padded_scaled = np.zeros(img.shape, dtype=np.uint8)
        padded_scaled[center_y:center_y + sh, center_x:center_x + sw] = result
        result = padded_scaled

    # cv2.imshow("Scaled padded/cropped image", result)
    # cv2.resizeWindow("Scaled padded/cropped image",500,500)
    # cv2.waitKey(0)
    return np.array(result[:,:,0])

def my_rotate_image(sample,angle):
    im = Image.new('L', (28, 28))
    im.putdata(sample)
    result = im.rotate(angle,resample=Image.BILINEAR)
    # result = im.rotate(angle, resample=Image.NEAREST)
    return np.array(result)

def elastic_transform(image, alpha, sigma, random_state=None):
    """Elastic deformation of images as described in [Simard2003]_.
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
       Convolutional Neural Networks applied to Visual Document Analysis", in
       Proc. of the International Conference on Document Analysis and
       Recognition, 2003.
    """
    if random_state is None:
        random_state = numpy.random.RandomState(None)

    shape = image.shape
    dx = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha
    dy = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha

    x, y = numpy.meshgrid(numpy.arange(shape[0]), numpy.arange(shape[1]))
    indices = numpy.reshape(y+dy, (-1, 1)), numpy.reshape(x+dx, (-1, 1))

    return map_coordinates(image, indices, order=1).reshape(shape)