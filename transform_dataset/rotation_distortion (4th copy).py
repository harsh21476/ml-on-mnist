import tensorflow as tf
# import cv2
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as linear_alg
import pandas
import my_lib
from sklearn.decomposition import PCA
from PIL import Image
import sklearn.covariance

# matrix_a=np.array([[1,14],[6,1],[23,11]])
# covariance=np.cov(matrix_a.T)
# print covariance
# w,v=linear_alg.eig(covariance)
# # print 'The determinent is: '
# # print np.linalg.det(covariance)
# print 'The eigen values are: '
# print w
# print 'The eigen vectores are: '
# print v
# # product=np.linalg.inv(v).dot(covariance).dot(v)
# # print product
#
# pca=PCA(n_components=2)
# # print 'After Transform: '
# pca.fit_transform(matrix_a)
# print 'The eigen vectors are: '
# print pca.components_.T
# # product=np.dot(np.linalg.inv(pca.components_.T),covariance)
# # product=np.dot(product,pca.components_.T)
# # print product
# # matrix_b=np.array([[1,2],[3,4]])
# # print matrix_a.dot(matrix_b)
# # x=[1,2,1,3,4,5]
# # y=[1,1,2,4,6,8]
# # plt.scatter(x,y)
# # plt.show()
# img=Image.open("image.png")
# img2=img.rotate(45)
# print img2.toarray()
# img2.show()


# img_scaled = cv2.resize(img,None,fx=1.2, fy=1.2, interpolation = cv2.INTER_LINEAR)
# cv2.imshow('Scaling - Linear Interpolation', img_scaled) img_scaled = cv2.resize(img,None,fx=1.2, fy=1.2, interpolation = cv2.INTER_CUBIC)
# cv2.imshow('Scaling - Cubic Interpolation', img_scaled) img_scaled = cv2.resize(img,(450, 400), interpolation = cv2.INTER_AREA)
# cv2.imshow('Scaling - Skewed Size', img_scaled) cv2.waitKey()

filename = 'Datasets/mnist_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)
print("reading Datasets done")
array = trainDataset.values
XTrain = array[:,1:785]
YTrain = array[:,0]
YTrain = np.reshape(YTrain,(59999,1))

array = testDataset.values
XTest = array[:,1:785]
YTest = array[:,0]
YTest = np.reshape(YTest,(9999,1))
print("loading Datasets done")

# ============================            Scaling                 ===========================
# im=Image.new('L',(28,28))
# im.putdata(XTrain[0,:])
# im.save('image.png')
# plt.imshow(im,cmap='gray')
# # plt.show()
#
# img=cv2.cvtColor(np.array(im),cv2.COLOR_GRAY2RGB)
# img = cv2.imread('image.png')
# res = cv2.resize(img,None,fx=10, fy=10, interpolation = cv2.INTER_CUBIC)
# res = cv2.resize(img,None,fx=10.5, fy=20.8, interpolation = cv2.INTER_NEAREST)
# # cv2.imshow('res',res)
# arr=np.array(res)
# print arr
# print arr.shape
# cv2.imshow('res1',res)
# cv2.waitKey(0)
# cv2.destroyAllWindows()




# import cv2
# import numpy as np
# ============================            Rotation                 ===========================

XTrain_temp=XTrain[10000:15000,:]
YTrain_temp=YTrain[10000:15000,:]
XTrain_rotation=XTrain[0, :]
YTrain_rotation=YTrain[0, :]
i=0
print("1")
for sample in XTrain_temp:
    print(i)
    if ((YTrain_temp[i]==1) or (YTrain_temp[i]==7)):
        rotate_sample = np.random.uniform(low=-7.5, high=7.0, size=(3,))
    else:
        rotate_sample = np.random.uniform(low=-15.0, high=15.0, size=(3,))
    # scalex_sample = np.random.uniform(low=0.82, high=1.18, size=(3,))
    # scaley_sample = np.random.uniform(low=0.82, high=1.18, size=(3,))
    # elastic_alpha_sample = np.random.uniform(low=36.0, high=38.0, size=(3,))
    # elastic_sigma_sample = np.random.uniform(low=5.0, high=6.0, size=(3,))
    for j in range(0,3):
        # scale_result=my_lib.my_scale_image(sample,scalex_sample[j],scaley_sample[j])
        rotate_result=my_lib.my_rotate_image(sample,rotate_sample[j])
        # elastic_result=my_lib.elastic_transform(np.reshape(sample,(28,28)), elastic_alpha_sample[j], elastic_sigma_sample[j], random_state=None)
        # XTrain_elastic = np.vstack((XTrain_elastic,np.reshape(elastic_result,(1,784))))
        XTrain_rotation = np.vstack((XTrain_rotation,np.reshape(rotate_result,(1,784))))
        # XTrain_rotation = np.vstack((XTrain_rotation,np.reshape(rotate_result,(1,784)) ,np.reshape(scale_result,(1,784)),np.reshape(elastic_result,(1,784))))
        # XTrain_rotation = np.vstack((XTrain, np.reshape(rotate_result, (1, 784)), np.reshape(scale_result, (1, 784))))
        YTrain_rotation = np.vstack((YTrain_rotation,np.reshape(YTrain_temp[i],(1,1))))
        # YTrain_elastic = np.vstack((YTrain_elastic,np.reshape(YTrain_temp[i], (1, 1))))
        # YTrain_elastic = np.vstack((YTrain_elastic, np.reshape(YTrain_temp[i], (1, 1))))
    i+=1

# A=np.array([[1,2,3],[4,5,6]])
# B=np.array([[7],[8]])
temp=np.hstack((XTrain_rotation,YTrain_rotation))
print(XTrain_rotation.shape)
print(YTrain_rotation.shape)
np.savetxt("mnist_rotation_distorted_03.csv",temp[1:,:], delimiter=",",fmt='%d')

# filename = 'mnist_distorted.csv'
# trainDataset = pandas.read_csv(filename,delimiter=',')
# array = trainDataset.values
# trainDataset = array
# print trainDataset.shape
# print trainDataset
# print trainDataset[0,:]
# print XTrain.shape
# print YTrain.shape