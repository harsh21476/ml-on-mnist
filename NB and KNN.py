import time
start_time = time.time()


import pandas
import sys
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import KFold
from sklearn.svm import SVC

filename = 'Datasets/mnist_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)

array = trainDataset.values
XTrain = array[:,1:785]
YTrain = array[:,0]
# print(XTrain.shape)
# print(YTrain.shape)

array = testDataset.values
XTest = array[1:4,1:785]
YTest = array[1:4,0]


# kf = KFold(n_splits=10)
# kf.get_n_splits(X)
# knn_accucray_array =[]
# for train_index, test_index in kf.split(X):
    # print ("train and test")
    # print train_index
    # print test_index
knn = KNeighborsClassifier(n_neighbors=1)
knn.fit(XTrain,YTrain)
print("KNN accuracy score below:  ")

testPredictions = knn.predict(XTest)
print("test accuracy_score", accuracy_score(YTest, testPredictions)*100)
print(confusion_matrix(YTest, testPredictions))

print("in the end")
print("--- %s seconds ---" % (time.time() - start_time))