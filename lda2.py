import pandas as pd
import numpy as np
import sys
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
import math
# %matplotlib inline
np.set_printoptions(threshold=np.inf)


filename = 'Datasets/mnist_train.csv'
trainDataset = pd.read_csv(filename,skipinitialspace=True)

trainDataset.tail()

array = trainDataset.values
XTrain = array[:,1:784]
YTrain = array[:,0]

# X = scale(XTrain)
lda = LinearDiscriminantAnalysis(n_components=2)
lda.fit(XTrain, YTrain)
X_reduced_train = lda.transform(XTrain)
print('calculated X_reduced_train')
X_reduced_test = lda.transform(XTest)
sys.exit()
# =========================================================================
var= pca.explained_variance_
# var1=np.cumsum(np.round(pca.explained_variance_ratio_, decimals=4)*100)
# print(np.sort(var))
# print(pca.components_ )
print(var)
print(len(var))
print("calculated var...moving forward")
pca.n_components = 50

X_reduced = pca.fit_transform(XTrain)
#print(X_reduced.shape)
#print(X_reduced)

sys.exit()
# print(var1)

plt.plot(var1)

pca = PCA(n_components=30)
pca.fit(X)
X1=pca.fit_transform(X)

print(X1)