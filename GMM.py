import pandas
import sys
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
# from sklearn.tree import DecisionTreeClassifier
# from sklearn.neighbors import KNeighborsClassifier
# from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
# from sklearn.naive_bayes import GaussianNB
# from sklearn.svm import SVC
from sklearn.mixture import GaussianMixture

filename = 'Datasets/mnist_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)

array = trainDataset.values
XTrain = array[:,1:785]
YTrain = array[:,0]
print(XTrain.shape)
print(YTrain.shape)

array = testDataset.values
XTest = array[1:50,1:785]
YTest = array[1:50,0]

print("GMM")
gmm = GaussianMixture(n_components=10)
# knn = KNeighborsClassifier(n_neighbors=30)
gmm.fit(XTrain)
# knn.fit(XTrain,YTrain)
predictions = gmm.predict(XTest)
print(accuracy_score(YTest, predictions))


print("in the end")
